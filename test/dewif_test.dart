import 'package:durt/src/crypto/dewif.dart';
import 'package:test/test.dart';

void main() {
  group('wallets', () {
    test('Create dewif and unlock it', () async {
      print('\n------------------\n DEWIF Test\n------------------\n');

      Dewif _dewif = Dewif();
      final mnemonicRFC =
          "crop cash unable insane eight faith inflict route frame loud box vibrant";
      final _dewifDataRFC = await _dewif.generateDewif(
          mnemonicRFC, 'toto titi tata',
          lang: 'english',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST,
          testRfc: true);
      print(_dewifDataRFC.dewif);

      String? decryptedDewifRFC;
      try {
        decryptedDewifRFC = _dewif.mnemonicFromDewif(
            _dewifDataRFC.dewif, _dewifDataRFC.password,
            lang: 'english', dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
        print('Unlock: ' + decryptedDewifRFC);
      } on ChecksumException {
        print('Bad secret code');
      } catch (e) {
        print(e);
      }
      expect(
          decryptedDewifRFC?.trim(),
          equals(
              'crop cash unable insane eight faith inflict route frame loud box vibrant'));

      // Change Dewif password
      final changeDewifPassword = await _dewif.changePassword(
          dewif: _dewifDataRFC.dewif,
          oldPassword: _dewifDataRFC.password,
          newPassword: 'NBVCX',
          lang: 'english',
          dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);

      expect(changeDewifPassword.password, 'NBVCX');

      final unlockChangeDewifPassword = _dewif.mnemonicFromDewif(
          changeDewifPassword.dewif, 'NBVCX',
          lang: 'english', dewifCurrencyCode: DEWIF_CURRENCY_CODE_G1_TEST);
      expect(unlockChangeDewifPassword,
          'crop cash unable insane eight faith inflict route frame loud box vibrant');
    });
  });
}
