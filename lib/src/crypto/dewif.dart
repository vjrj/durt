import 'dart:math';
import 'dart:convert';
import 'package:bip32_ed25519/api.dart';
import 'package:pointycastle/digests/sha256.dart';
import 'package:pointycastle/pointycastle.dart';
import "package:hex/hex.dart";
import 'package:bip39_multi_nullsafety/bip39_multi_nullsafety.dart' as bip39;
// import 'package:crypto/crypto.dart' as crypto;

// ignore_for_file: constant_identifier_names
const DEWIF_VERSION = 0x01;
const DEWIF_CURRENCY_CODE_G1 = 0x00000001;
const DEWIF_CURRENCY_CODE_G1_TEST = 0x10000001;
const DEWIF_ALGORITHM_CODE_BIP32_ED25519 = 0x01;
const DEWIF_MNEMONIC_LANGUAGES = {
  0: "english",
  1: "chinese_simplified",
  2: "chinese_traditional",
  3: "french",
  4: "italian",
  5: "japanese",
  6: "korean",
  7: "spanish",
};

class Dewif {
  // var sha256 = SHA256Digest();
  Uint8List sha256(List<int> data) =>
      // Uint8List.fromList(crypto.sha256.convert(data).bytes);
      SHA256Digest().process(Uint8List.fromList(data));

  /// Binary XOR encryption
  Uint8List byteXor(Uint8List data, Uint8List key) {
    List<int> output = [];

    for (var i = 0; i < data.length; i++) {
      var charCode = data[i] ^ key[i % key.length];
      output.add(charCode);
    }

    return output.toUint8List();
  }

  /// Return a random byte of the given size
  String randomByte(int size) {
    var random = Random.secure();
    return HEX.encode(
        List<int>.generate(size, (i) => random.nextInt(256)).toUint8List());
  }

  /// Return the base64 DEWIF string generated from given mnemonic and password.
  /// You can optionnaly specify a custom parameters, as language for mnemonic (default is enhlish)
  /// More detail in the Duniter RFC13: https://git.duniter.org/documents/rfcs/blob/master/rfc/0013_Duniter_Encrypted_Wallet_Import_Format.md
  Future<NewWallet> generateDewif(String mnemonic, String password,
      {String lang = 'english',
      int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION,
      bool testRfc = false}) async {
    mnemonic = mnemonic.replaceAll('é', 'é');
    mnemonic = mnemonic.replaceAll('è', 'è');
    if (testRfc) print(mnemonic);

    const int log_n = 14;

    var nonce = randomByte(12);

    // To test RFC
    if (testRfc) nonce = 'c54299ae71fe2a4ecdc7d58a';

    // Header is data version + currency code on 4 bytes
    final header = (dewifVersion.toRadixString(16).padLeft(8, '0') +
        dewifCurrencyCode.toRadixString(16).padLeft(8, '0'));

    // Start data with header + log_n + used algorithm code + nonce
    var data = header +
        log_n.toRadixString(16).padLeft(2, '0') +
        DEWIF_ALGORITHM_CODE_BIP32_ED25519.toRadixString(16).padLeft(2, '0') +
        nonce;

    // Salt is sha256("dewif" || nonce || passphrase)
    final salt =
        sha256("dewif".codeUnits + HEX.decode(nonce) + password.codeUnits);

    final scrypt = KeyDerivator('scrypt');
    scrypt.init(
      ScryptParameters(
        pow(2, log_n).toInt(), //16384
        16,
        1,
        42,
        salt.toUint8List(),
      ),
    );
    final scryptedPassword = scrypt.process(password.codeUnits.toUint8List());

    final entropyMnemonic =
        HEX.decode(bip39.mnemonicToEntropy(mnemonic, language: lang));

    var entropyPadding = randomByte(32 - entropyMnemonic.length);

    // To test RFC
    if (testRfc) entropyPadding = 'aa083bd16c8317121d34b5aed1c1420a';

    final languageCode = DEWIF_MNEMONIC_LANGUAGES.keys
        .firstWhere((k) => DEWIF_MNEMONIC_LANGUAGES[k] == lang);

    final checksum = sha256(HEX.decode(nonce) +
            [languageCode] +
            [entropyMnemonic.length] +
            entropyMnemonic)
        .sublist(0, 8);

    final dataToEncrypt = languageCode.toRadixString(16) +
        entropyMnemonic.length.toRadixString(16) +
        HEX.encode(entropyMnemonic) +
        entropyPadding +
        HEX.encode(checksum);

    final encryptedData = byteXor(
        Uint8List.fromList(HEX.decode(dataToEncrypt)), scryptedPassword);

    data = data + HEX.encode(encryptedData);
    final base64Data = base64.encode(HEX.decode(data));

    //To test RFC
    if (testRfc &&
        base64Data ==
            'AAAAARAAAAEOAcVCma5x/ipOzcfViufNdfj5k4Sl5zdrHLf9PPGDkH1Pz3y8tFrx/jZZcJd92LIk+EWIrjxiSw==') {
      print('Data is conform to RFC !');
    }

    // Show me everything
    // print('\nVersion: ' + dewifVersion.toRadixString(16));
    // print('log N: ' + log_n.toRadixString(16));
    // print('Algorithm: ' + DEWIF_ALGORITHM_CODE_BIP32_ED25519.toRadixString(16));
    // print('Nonce: ' + nonce);
    // print('Language: ' + languageCode.toRadixString(16));
    // print('Entropy length: ' + entropyMnemonic.length.toRadixString(16));
    // print('Entropy Mnemonic: ' + HEX.encode(entropyMnemonic));
    // print('Entropy padding: ' + entropyPadding);
    // print('Checksum: ' + HEX.encode(checksum));
    // print(data);

    return NewWallet._(base64Data, password);
  }

  /// Decrypt the given DEWIF in base64 string format and associated password.
  /// Return error if DEWIF is corrupted, or the password is not the good one.
  /// If success, return mnemonic.
  String mnemonicFromDewif(String dewif, String password,
      {String lang = 'english',
      int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION,
      bool testRfc = false}) {
    const int log_n = 14;

    bool isValid = verifyDewif(dewif,
        dewifCurrencyCode: dewifCurrencyCode, dewifVersion: dewifVersion);

    if (!isValid) {
      throw Exception('Error: Dewif is corrupted');
    }

    final rawDewif = HEX.encode(base64.decode(dewif));
    final nonce = rawDewif.substring(20, 44);
    final contentData = rawDewif.substring(44);

    final salt =
        sha256("dewif".codeUnits + HEX.decode(nonce) + password.codeUnits);

    final scrypt = KeyDerivator('scrypt');
    scrypt.init(
      ScryptParameters(
        pow(2, log_n).toInt(), //16384
        16,
        1,
        42,
        salt.toUint8List(),
      ),
    );
    final scryptedPassword = scrypt.process(password.codeUnits.toUint8List());

    final decryptedData = HEX.encode(
        byteXor(HEX.decode(contentData).toUint8List(), scryptedPassword));

    final decryptedLang = decryptedData.substring(0, 2);
    final decryptedEntropyLength = decryptedData.substring(2, 4);
    final mnemonicEntropy = decryptedData.substring(4, 36);
    final decryptedCheksum = decryptedData.substring(68);

    final builtChecksum = HEX.encode(sha256(HEX.decode(nonce) +
            HEX.decode(decryptedLang) +
            HEX.decode(decryptedEntropyLength) +
            HEX.decode(mnemonicEntropy))
        .sublist(0, 8));

    if (decryptedCheksum != builtChecksum) {
      throw ChecksumException('Error: bad checksum');
    } else {
      return bip39.entropyToMnemonic(mnemonicEntropy, language: lang);
    }
  }

  /// Return true if the given DEWIF base64 string is valid.
  bool verifyDewif(String dewif,
      {int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION}) {
    String dewifCurrencyCodeHexa =
        dewifCurrencyCode.toRadixString(16).padLeft(8, '0');
    String dewifVersionHexa = dewifVersion.toRadixString(16).padLeft(8, '0');

    final rawDewif = HEX.encode(base64.decode(dewif));
    final version = rawDewif.substring(0, 8);
    final currencyCode = rawDewif.substring(8, 16);
    final logN = rawDewif.substring(16, 18);
    final algorythm = rawDewif.substring(18, 20);

    // print(
    //     "$version == $dewifVersion && $currencyCode == $dewifCurrencyCodeHexa && $logN == '0e' && $algorythm == ${DEWIF_ALGORITHM_CODE_BIP32_ED25519.toRadixString(16).padLeft(2, '0')}");

    if (version == dewifVersionHexa &&
        currencyCode == dewifCurrencyCodeHexa &&
        logN == '0e' &&
        algorythm ==
            DEWIF_ALGORITHM_CODE_BIP32_ED25519
                .toRadixString(16)
                .padLeft(2, '0')) {
      return true;
    } else {
      return false;
    }
  }

  bool isNumeric(string) => num.tryParse(string) != null;

  Future<NewWallet> changePassword(
      {required String dewif,
      required String oldPassword,
      String? newPassword,
      String lang = 'english',
      int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION}) async {
    final mnemonic = mnemonicFromDewif(dewif, oldPassword,
        lang: lang,
        dewifCurrencyCode: dewifCurrencyCode,
        dewifVersion: dewifVersion);
    newPassword ??=
        randomSecretCode(oldPassword.length, isNum: isNumeric(oldPassword));

    return await generateDewif(mnemonic, newPassword,
        lang: lang,
        dewifCurrencyCode: dewifCurrencyCode,
        dewifVersion: dewifVersion);
  }

  Future<NewWallet> changeCesiumPassword(
      {required String dewif,
      required String oldPassword,
      String? newPassword,
      int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION}) async {
    final seed = cesiumSeedFromDewif(dewif, oldPassword,
        dewifCurrencyCode: dewifCurrencyCode, dewifVersion: dewifVersion);
    newPassword ??=
        randomSecretCode(oldPassword.length, isNum: isNumeric(oldPassword));

    return await generateCesiumDewif(seed, newPassword,
        dewifCurrencyCode: dewifCurrencyCode, dewifVersion: dewifVersion);
  }

  /// Return the base64 DEWIF string generated from given seed and password.
  Future<NewWallet> generateCesiumDewif(Uint8List seed, String password,
      {int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION,
      bool test = false}) async {
    if (test) print('Seed: ' + seed.toString());

    const int log_n = 14;

    var nonce = randomByte(12);

    // To test
    if (test) nonce = 'c54299ae71fe2a4ecdc7d58a';

    // Header is data version + currency code on 4 bytes
    final header = (dewifVersion.toRadixString(16).padLeft(8, '0') +
        dewifCurrencyCode.toRadixString(16).padLeft(8, '0'));

    // Start data with header + log_n + used algorithm code + nonce
    var data = header +
        log_n.toRadixString(16).padLeft(2, '0') +
        DEWIF_ALGORITHM_CODE_BIP32_ED25519.toRadixString(16).padLeft(2, '0') +
        nonce;

    // Salt is sha256("dewif" || nonce || passphrase)
    final salt =
        sha256("dewif".codeUnits + HEX.decode(nonce) + password.codeUnits);

    final scrypt = KeyDerivator('scrypt');
    scrypt.init(
      ScryptParameters(
        pow(2, log_n).toInt(), //16384
        16,
        1,
        42,
        salt.toUint8List(),
      ),
    );
    final scryptedPassword = scrypt.process(password.codeUnits.toUint8List());

    var seedPadding = randomByte(40 - seed.length);

    // To test
    if (test) seedPadding = 'b34b1ac4c297c238';

    final checksum =
        sha256(HEX.decode(nonce) + [seed.length] + seed).sublist(0, 8);

    final dataToEncrypt = seed.length.toRadixString(16) +
        HEX.encode(seed) +
        seedPadding +
        HEX.encode(checksum);

    final encryptedData = byteXor(
        Uint8List.fromList(HEX.decode(dataToEncrypt)), scryptedPassword);

    data = data + HEX.encode(encryptedData);
    final base64Data = base64.encode(HEX.decode(data));

    //To test
    if (test &&
        base64Data ==
            'AAAAARAAAAEOAcVCma5x/ipOzcfViv8J5cdgASn7zedlNz6M/19wbE79aNvILr1CEyM664m8BjgLKeWiz1GYJSR14+ZY61U=') {
      print('Data is conform to test !');
    }

    // Show me everything
    // print('\nVersion: ' + dewifVersion.toRadixString(16));
    // print('log N: ' + log_n.toRadixString(16));
    // print('Algorithm: ' + DEWIF_ALGORITHM_CODE_BIP32_ED25519.toRadixString(16));
    // print('Nonce: ' + nonce);
    // print('Seed length: ' + seed.length.toRadixString(16));
    // print('Seed: ' + HEX.encode(seed));
    // print('Seed padding: ' + seedPadding);
    // print('Checksum: ' + HEX.encode(checksum));
    // print(data);

    return NewWallet._(base64Data, password);
  }

  /// Decrypt the given Cesium DEWIF in base64 string format and associated password.
  /// Return error if DEWIF is corrupted, or the password is not the good one.
  /// If success, return seed.
  Uint8List cesiumSeedFromDewif(String dewif, String password,
      {int dewifCurrencyCode = DEWIF_CURRENCY_CODE_G1,
      int dewifVersion = DEWIF_VERSION,
      bool test = false}) {
    const int log_n = 14;

    bool isValid = verifyDewif(dewif,
        dewifCurrencyCode: dewifCurrencyCode, dewifVersion: dewifVersion);

    if (!isValid) {
      throw Exception('Error: Dewif is corrupted');
    }

    final rawDewif = HEX.encode(base64.decode(dewif));
    final nonce = rawDewif.substring(20, 44);
    final contentData = rawDewif.substring(44);

    final salt =
        sha256("dewif".codeUnits + HEX.decode(nonce) + password.codeUnits);

    final scrypt = KeyDerivator('scrypt');
    scrypt.init(
      ScryptParameters(
        pow(2, log_n).toInt(), //16384
        16,
        1,
        42,
        salt.toUint8List(),
      ),
    );
    final scryptedPassword = scrypt.process(password.codeUnits.toUint8List());

    final decryptedData = HEX.encode(
        byteXor(HEX.decode(contentData).toUint8List(), scryptedPassword));

    final decryptedSeedLength = decryptedData.substring(0, 2);
    final seed = decryptedData.substring(2, 66);
    final decryptedCheksum = decryptedData.substring(82);

    final builtChecksum = HEX.encode(sha256(HEX.decode(nonce) +
            HEX.decode(decryptedSeedLength) +
            HEX.decode(seed))
        .sublist(0, 8));

    // print(decryptedSeedLength);
    // print(seed);
    // print("$decryptedCheksum != $builtChecksum");

    if (decryptedCheksum != builtChecksum) {
      throw ChecksumException('Error: bad checksum');
    } else {
      return Uint8List.fromList(HEX.decode(seed));
    }
  }
}

String randomSecretCode(int len, {bool isNum = false}) {
  final r = Random.secure();
  final String _chars;
  if (isNum) {
    _chars = '0123456789';
  } else {
    _chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }
  return List.generate(len, (index) => _chars[r.nextInt(_chars.length)]).join();
}

class ChecksumException implements Exception {
  String cause;
  ChecksumException(this.cause);
}

class NewWallet {
  final String dewif;
  final String password;

  NewWallet._(this.dewif, this.password);
}
