import 'package:bip32_ed25519/api.dart';
import 'package:durt/durt.dart';
import 'package:durt/src/gva/queries.dart';
import 'package:durt/src/gva/transaction.dart';
import 'package:graphql/client.dart';

enum DocumentSection { none, issuers, outputs, inputs, unlocks }

class ComplexTransaction {
  final List<Map<String, dynamic>> recipientsMap;
  final List<String> recipients;
  final HdWallet? wallet;
  final CesiumWallet? cesiumWallet;
  final int derivation;
  final String issuerPubkey;
  final int amount;
  final List<int> amounts;
  final String comment;
  final GraphQLClient client;
  bool useMempool;
  bool isChange = false;

  ComplexTransaction._({
    required this.recipients,
    required this.amount,
    required this.amounts,
    required this.wallet,
    required this.derivation,
    required this.cesiumWallet,
    required this.issuerPubkey,
    required this.client,
    this.comment = '',
    this.useMempool = false,
  }) : recipientsMap = _generateRecipientMap(recipients, amounts);

  // Main construtor transform dewif + password to hdwallet, or to cesiumWallet if derivation == -1
  // Priority to mnemonic or cesiumId+cesiumPwd, then seed, and final dewif+password
  factory ComplexTransaction(
      {required List<String> recipients,
      required List<int> amounts,
      required int amount,
      required String? dewif,
      required String? password,
      required String? mnemonic,
      required String? cesiumId,
      required String? cesiumPwd,
      required Uint8List? cesiumSeed,
      required GraphQLClient client,
      required int derivation,
      String lang = 'english',
      String comment = '',
      required useMempool}) {
    if (derivation == -1) {
      final CesiumWallet cesiumWallet;
      if (cesiumId != null && cesiumPwd != null) {
        cesiumWallet = CesiumWallet(cesiumId, cesiumPwd);
      } else if (cesiumSeed != null) {
        cesiumWallet = CesiumWallet.fromSeed(cesiumSeed);
      } else {
        cesiumWallet = CesiumWallet.fromDewif(dewif!, password!);
      }
      return ComplexTransaction._(
          recipients: recipients,
          amount: amount,
          amounts: amounts,
          wallet: null,
          derivation: derivation,
          issuerPubkey: cesiumWallet.pubkey,
          comment: comment,
          client: client,
          useMempool: useMempool,
          cesiumWallet: cesiumWallet);
    } else {
      final HdWallet wallet;
      if (mnemonic != null) {
        wallet = HdWallet.fromMnemonic(mnemonic);
      } else {
        wallet = HdWallet.fromDewif(dewif!, password!, lang: lang);
      }
      return ComplexTransaction._(
          recipients: recipients,
          amount: amount,
          amounts: amounts,
          wallet: wallet,
          derivation: derivation,
          issuerPubkey: wallet.getPubkey(derivation),
          comment: comment,
          client: client,
          useMempool: useMempool,
          cesiumWallet: null);
    }
  }
  static List<Map<String, dynamic>> _generateRecipientMap(
      List<String> recipients, List<int> amount) {
    List<Map<String, dynamic>> recipientsMap = [];
    for (int i = 0; i < recipients.length; i++) {
      recipientsMap
          .add({'script': 'SIG(${recipients[i]})', 'amount': amount[i]});
    }
    return recipientsMap;
  }

  Future<List> _generateComplexTransactionDocument() async {
    // Check if we try to send money to ourself.
    // This is not allow, because that will be interpreted as a change document,
    // and create hell's infinite loop.
    if (recipients.contains(issuerPubkey)) {
      throw MySelfException("You can't send money to yourself.");
    }

    List<Map<String, dynamic>> issuers = [];

    issuers.add(createIssuer(amount, issuerPubkey));

    final QueryOptions options = QueryOptions(
      document: gql(genComplexTransactionDocQuery),
      variables: <String, dynamic>{
        'issuers': issuers,
        'recipients': recipientsMap,
        'comment': comment,
        'useMempool': useMempool
      },
    );

    final QueryResult result = await client.query(options);

    if (result.hasException) {
      throw GraphQLException('GraphQL error: ${result.exception.toString()}');
    } else {
      // change?
      return [result.data!['genComplexTx']['tx']];
    }
  }

  bool _checkTransactionDocument(List transDoc) {
    List<String> issuersList = [];
    List<String> recipientsList = [];
    List<int> amountsList = [];
    List<String> commentsList = [];

    DocumentSection currentSection = DocumentSection.none;

    for (final String doc in transDoc) {
      List<String> docList = doc.split('\n');

      for (String line in docList) {
        if (line == 'Issuers:') {
          currentSection = DocumentSection.issuers;
        } else if (line == 'Outputs:') {
          currentSection = DocumentSection.outputs;
        } else if (line == 'Inputs:') {
          currentSection = DocumentSection.inputs;
        } else if (line == 'Unlocks:') {
          currentSection = DocumentSection.unlocks;
        } else if (line.startsWith('Comment: ')) {
          commentsList.add(line.substring('Comment: '.length));
          currentSection = DocumentSection.none;
        } else {
          switch (currentSection) {
            case DocumentSection.issuers:
              if (line.trim().isNotEmpty) {
                issuersList.add(line);
              }
              break;
            case DocumentSection.outputs:
              if (line.trim().isNotEmpty) {
                List<String> outputParts = line.split(':');
                if (outputParts.length >= 3) {
                  int amount = int.tryParse(outputParts[0]) ?? 0;
                  amountsList.add(amount);
                  String recipientScript = outputParts[2];
                  if (recipientScript.startsWith('SIG(')) {
                    recipientScript =
                        recipientScript.split('SIG(')[1].replaceAll(')', '');
                  }
                  recipientsList.add(recipientScript);
                }
              }
              break;
            case DocumentSection.inputs:
            case DocumentSection.unlocks:
            case DocumentSection.none:
              break;
          }
        }
      }
    }

    // Check if it's a change transaction document
    if (recipientsList.every((element) => element == issuerPubkey)) {
      print("The document contains a change transaction");
      isChange = true;
      useMempool = true;
      return true;
    } else {
      /* final int totalAmount =
          amountsList.fold(0, (sum, current) => sum + current);

      if (totalAmount != amount) {
        print('The total amount is incorrect');
        return false;
      }*/
      if (issuersList.any((element) => element != issuerPubkey)) {
        print(
            'There is an issuer that does not match the expected issuerPubkey');
        return false;
      }
      if (!recipientsList.any((element) => recipients.contains(element))) {
        print(
            'There is a recipient that does not match the expected recipients');
        return false;
      }
      if (!commentsList.contains(comment)) {
        print('The comment does not match the expected comment');
        return false;
      }
      isChange = false;
      return true;
    }
  }

  List _signDocument(List documents) {
    List<String> signedDocuments = [];
    for (String doc in documents) {
      String signature;
      if (derivation == -1) {
        signature = cesiumWallet!.sign(doc);
      } else {
        signature = wallet!.sign(doc, derivation: derivation);
      }
      signedDocuments.add(doc + signature);
      // print(signedDocuments);
    }
    return signedDocuments;
  }

  Future<bool> _sendSignedDocuments(List signedDocs) async {
    // print(signedDocs);
    for (String doc in signedDocs) {
      final QueryOptions options = QueryOptions(
        document: gql(sendTransactionDocQuery),
        variables: <String, String>{
          'signedDoc': doc,
        },
      );
      final QueryResult result = await client.query(options);

      if (result.hasException) {
        throw GraphQLException('GraphQL error: ${result.exception.toString()}');
      } else if (isChange) {
        await process();
        return true;
      } else {
        print('Transaction complete !');
        return true;
      }
    }
    return false;
  }

  Future<String> process([bool raiseException = false]) async {
    try {
      final List transDocs = await _generateComplexTransactionDocument();
      if (_checkTransactionDocument(transDocs)) {
        final List signedDocs = _signDocument(transDocs);
        final bool result = await _sendSignedDocuments(signedDocs);
        return result ? 'success' : 'Error with transaction document';
      } else {
        return 'Transaction document is not valid.';
      }
    } on GraphQLException catch (e) {
      if (raiseException) {
        rethrow;
      }
      List<String> eCause = e.cause.split('message: ');
      return eCause.isNotEmpty
          ? eCause[eCause.length > 1 ? 1 : 0].split(',')[0]
          : 'Transaction failed for unknown reason';
    } on MySelfException catch (e) {
      return e.cause;
    }
  }

  Map<String, dynamic> createIssuer(int amount, String publicKey) {
    return {
      // 'script': publicKey,
      'signers': [publicKey],
      // 'codes': [],
      'amount': amount
    };
  }
}
