import 'package:bip32_ed25519/api.dart';
import 'package:collection/collection.dart';
import 'package:durt/durt.dart';
import 'package:durt/src/gva/queries.dart';
import 'package:graphql/client.dart';

class Transaction {
  final String recipient;
  final HdWallet? wallet;
  final CesiumWallet? cesiumWallet;
  final int derivation;
  final String issuerPubkey;
  final int amount;
  final String comment;
  final GraphQLClient client;
  bool useMempool;
  bool isChange = false;

  Transaction._({
    required this.recipient,
    required this.amount,
    required this.wallet,
    required this.derivation,
    required this.cesiumWallet,
    required this.issuerPubkey,
    required this.client,
    this.comment = '',
    this.useMempool = false,
  });

  // Main construtor transform dewif + password to hdwallet, or to cesiumWallet if derivation == -1
  // Priority to mnemonic or cesiumId+cesiumPwd, then seed, and final dewif+password
  factory Transaction({
    required String recipient,
    required int amount,
    required String? dewif,
    required String? password,
    required String? mnemonic,
    required String? cesiumId,
    required String? cesiumPwd,
    required Uint8List? cesiumSeed,
    required GraphQLClient client,
    required int derivation,
    String lang = 'english',
    String comment = '',
    required useMempool,
  }) {
    if (derivation == -1) {
      final CesiumWallet cesiumWallet;
      if (cesiumId != null && cesiumPwd != null) {
        cesiumWallet = CesiumWallet(cesiumId, cesiumPwd);
      } else if (cesiumSeed != null) {
        cesiumWallet = CesiumWallet.fromSeed(cesiumSeed);
      } else {
        cesiumWallet = CesiumWallet.fromDewif(dewif!, password!);
      }
      return Transaction._(
          recipient: recipient,
          amount: amount,
          wallet: null,
          derivation: derivation,
          issuerPubkey: cesiumWallet.pubkey,
          comment: comment,
          client: client,
          useMempool: useMempool,
          cesiumWallet: cesiumWallet);
    } else {
      final HdWallet wallet;
      if (mnemonic != null) {
        wallet = HdWallet.fromMnemonic(mnemonic);
      } else {
        wallet = HdWallet.fromDewif(dewif!, password!, lang: lang);
      }
      return Transaction._(
          recipient: recipient,
          amount: amount,
          wallet: wallet,
          derivation: derivation,
          issuerPubkey: wallet.getPubkey(derivation),
          comment: comment,
          client: client,
          useMempool: useMempool,
          cesiumWallet: null);
    }
  }

  Future<List> _generateTransactionDocument() async {
    // Check if we try to send money to ourself.
    // This is not allow, because that will be interpreted as a change document,
    // and create hell's infinite loop.
    if (issuerPubkey == recipient) {
      throw MySelfException("You can't send money to yourself.");
    }

    final QueryOptions options = QueryOptions(
      document: gql(genTransactionDocQuery),
      variables: <String, dynamic>{
        'recipient': recipient,
        'amount': amount,
        'issuer': issuerPubkey,
        'comment': comment,
        'useMempool': useMempool
      },
    );
    final QueryResult result = await client.query(options);

    if (result.hasException) {
      throw GraphQLException('GraphQL error: ${result.exception.toString()}');
    } else {
      return result.data!['genTx'];
    }
  }

  bool _checkTransactionDocument(List transDoc) {
    List<String> issuersList = [];
    List<String> recipientsList = [];
    // List outputsList = [];
    List<int> amountsList = [];
    List<String> commentsList = [];

    // Parse received transaction document
    for (String doc in transDoc) {
      List<String> docList = doc.split('\n');

      docList.asMap().forEach((i, line) {
        if (line == 'Issuers:') {
          issuersList.add(docList[i + 1]);
        } else if (line == 'Outputs:') {
          List<String> outputList = docList[i + 1].split(':');
          amountsList.add(int.parse(outputList[0]));
          recipientsList
              .add(outputList[2].split('SIG(')[1].replaceFirst(')', ''));
        } else if (line.startsWith('Comment: ')) {
          commentsList.add(line.split(': ')[1]);
        }
      });
    }
    // Check if it's a change transaction document
    if (recipientsList.every((element) => element == issuerPubkey)) {
      print("The document contain a change transaction");
      isChange = useMempool = true;
      return true;
    } else if (issuersList.every((element) => element != issuerPubkey) ||
        amountsList.sum != amount ||
        recipientsList.every((element) =>
            element != recipient ||
            commentsList.every((element) => element != comment))) {
      // Check validity of transaction document
      print('The generated document is corrupted');
      return false;
    } else {
      isChange = false;
      return true;
    }
  }

  List _signDocument(List documents) {
    List<String> signedDocuments = [];
    for (String doc in documents) {
      String signature;
      if (derivation == -1) {
        signature = cesiumWallet!.sign(doc);
      } else {
        signature = wallet!.sign(doc, derivation: derivation);
      }
      signedDocuments.add(doc + signature);
      // print(signedDocuments);
    }
    return signedDocuments;
  }

  Future<bool> _sendSignedDocuments(List signedDocs) async {
    // print(signedDocs);
    for (String doc in signedDocs) {
      final QueryOptions options = QueryOptions(
        document: gql(sendTransactionDocQuery),
        variables: <String, String>{
          'signedDoc': doc,
        },
      );
      final QueryResult result = await client.query(options);

      if (result.hasException) {
        throw GraphQLException('GraphQL error: ${result.exception.toString()}');
      } else if (isChange) {
        await process();
        return true;
      } else {
        print('Transaction complete !');
        return true;
      }
    }
    return false;
  }

  Future<String> process([bool raiseException = false]) async {
    try {
      final List transDocs = await _generateTransactionDocument();
      if (_checkTransactionDocument(transDocs)) {
        final List signedDocs = _signDocument(transDocs);
        final bool result = await _sendSignedDocuments(signedDocs);
        return result ? 'success' : 'Error with transaction document';
      } else {
        return 'Transaction document is not valid.';
      }
    } on GraphQLException catch (e) {
      if (raiseException) {
        rethrow;
      }
      List<String> eCause = e.cause.split('message: ');
      return eCause.isNotEmpty
          ? eCause[eCause.length > 1 ? 1 : 0].split(',')[0]
          : 'Transaction failed for unknown reason';
    } on MySelfException catch (e) {
      return e.cause;
    }
  }
}

class GraphQLException implements Exception {
  String cause;
  GraphQLException(this.cause);
}

class MySelfException implements Exception {
  String cause;
  MySelfException(this.cause);
}
